(function () {
  'use strict';

  let data = [
    {
      "city": "Belgrade",
      "latitude": 44.8048,
      "longitude": 20.4781,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        },
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "Madrid",
      "latitude": 40.4167,
      "longitude": -3.7033,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        },
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "Algeria",
      "latitude": 28.033886,
      "longitude": 1.659626,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        }
      ]
    },
    {
      "city": "Stockholm",
      "latitude": 59.3328,
      "longitude": 18.0645,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        },
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "Rio Negro",
      "latitude": -3.162456,
      "longitude": -59.919434,
      "servers": [

        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "Novosibirsk",
      "latitude": 55.008353,
      "longitude": 82.935733,
      "servers": [
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "Tokyo",
      "latitude": 35.689488,
      "longitude": 139.691706,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        },
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "London",
      "country": "GB",
      "latitude": 51.5002,
      "longitude": -0.1262,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        }
      ]
    },
    {
      "city": "Prague",
      "latitude": 50.0878,
      "longitude": 14.4205,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        }
      ]
    },
    {
      "city": "Reykjavik",
      "latitude": 64.1353,
      "longitude": -21.8952,
      "servers": [
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "Oslo",
      "latitude": 59.9138,
      "longitude": 10.7387,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        },
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "New Delhi",
      "latitude": 28.613939,
      "longitude": 77.209021,
      "servers": [
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "Moscow",
      "latitude": 55.7558,
      "longitude": 37.6176,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        }
      ]
    },
    {
      "city": "Bilibino",
      "latitude": 68.059639,
      "longitude": 166.443893,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        },
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    },
    {
      "city": "Rio Negro",
      "latitude": -3.162456,
      "longitude": -59.919434,
      "servers": [
        {
          "city": "New York",
          "country": "USA",
          "latitude": 40.7127840,
          "longitude": -74.0059410
        },
        {
          "city": "San Francisco",
          "country": "USA",
          "latitude": 37.7749290,
          "longitude": -122.4194160
        }
      ]
    }
  ];

  let width = 556,
    height = 280;

  let projection = d3.geo.mercator()
    .precision(0)
    .translate([283, 180])
    .scale(90);

  let path = d3.geo.path()
    .projection(projection);

  let drag = d3.behavior.drag()
    .on("drag", draged);

  let svg = d3.select('body')
    .append('svg')
    .attr('id', 'main-svg')
    .attr('width', width)
    .attr('height', height)
    .call(drag);

  let container = svg.append('g')
    .attr('id', 'container');

  let countries = container.append('g')
    .attr('id', 'countries');

  let arcGroup = container.append('g')
    .attr('id', 'lines');

  let points = container.append('g')
    .attr('id', 'points');

  function draged() {
    projection.translate([
      projection.translate()[0] + d3.event.dx,
      projection.translate()[1] + d3.event.dy
    ]);
    svg.selectAll('path').attr('d', path);
    svg.selectAll('circle').attr('transform', function (d) {
      return 'translate(' + projection([
          d.longitude,
          d.latitude
        ]) + ')';
    });
  }

  d3.json('world-countries.json', function (collection) {
    countries.selectAll('path')
      .data(collection.features)
      .enter().append('svg:path')
      .attr('d', path);
  });

  let circle_array = points.selectAll('circle')
    .data(data);

  let attackersContainers = circle_array
    .data(data)
    .enter().append('g')
    .attr('class', 'attacker-container');

  var tooltip = d3.select('.tooltip');

  attackersContainers.append('circle')
    .attr('r', 5)
    .attr('class', 'attacker')
    .attr('transform', function (d) {
      return 'translate(' + projection([
          d.longitude,
          d.latitude
        ]) + ')';
    })
    .on('mouseover', function (d) {
      tooltip
        .style('left', (d3.event.pageX) + 20 + 'px')
        .style('top', (d3.event.pageY - 35) + 'px')
        .select('#attacks-count').html(d.servers.length);

      tooltip
        .select('#attacks-location').html(d.city);

      tooltip
        .style('opacity', 1);
    })
    .on('mouseout', function () {
      tooltip
        .style('left', -100 + 'px')
        .style('top', -100 + 'px')
        .style('opacity', 0);
    })
    .on('click', function () {
      // this.$state.go('')
      console.log('click')
    });

  attackersContainers.selectAll('.attacker-container')
    .data(function (d) {
      return d.servers;
    })
    .enter()
    .append('circle')
    .attr('r', 8)
    .attr('class', 'server')
    .attr('transform', function (d) {
      return 'translate(' + projection([
          d.longitude,
          d.latitude
        ]) + ')';
    });

// Lines

  let dataLines = [];

  _.map(data, function (attacker) {
    _.map(attacker.servers, function (server) {
      dataLines.push({
        type: 'LineString',

        coordinates: [
          [attacker.longitude, attacker.latitude],
          [server.longitude, server.latitude]
        ]
      });
    });

  });

  let pathArcs = arcGroup.selectAll('.arc')
    .data(dataLines);

  pathArcs
    .enter()
    .append('path')
    .attr({
      'class': 'arc'
    })
    .style({
      fill: 'none'
    });

  pathArcs
    .attr({
      d: path
    })
    .style({
      stroke: '#eb4a49',
      'stroke-width': '1px'
    });

  pathArcs.exit().remove();

  let scaleStep = 35;

  $('#plus').on('click', function () {
    if (projection.scale() <= 500) {
      let newTranslate = [projection.translate()[0] += scaleStep, projection.translate()[1] += scaleStep];
      let newScale = projection.scale() + scaleStep;

      updateAterZoom(newTranslate, newScale);
    }
  });

  $('#minus').on('click', function () {
    if (projection.scale() >= 56) {
      let newTranslate = [projection.translate()[0] -= scaleStep, projection.translate()[1] -= scaleStep];
      let newScale = projection.scale() - scaleStep;

      updateAterZoom(newTranslate, newScale);
    }
  });

  function updateAterZoom(newTranslate, newScale) {
    projection.translate(newTranslate).scale(newScale);

    svg
      .transition()
      .duration(300)
      .selectAll('path')
      .attr('d', path);

    svg
      .transition()
      .selectAll('circle')
      .attr('transform', function (d) {
        return 'translate(' + projection([
            d.longitude,
            d.latitude
          ]) + ')';
      });
  }

})
();
